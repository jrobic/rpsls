module.exports = {
  root: true,
  parser: "@typescript-eslint/parser",
  plugins: ["@typescript-eslint", "prettier", "jest"],
  globals: {
    Atomics: "readonly",
    SharedArrayBuffer: "readonly",
  },
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: "module",
  },
  env: {
    browser: true,
    es6: true,
    node: true,
    jest: true,
  },
  extends: [
    "airbnb",
    "airbnb/hooks",
    "plugin:@typescript-eslint/recommended",
    "prettier/@typescript-eslint",
    "plugin:prettier/recommended",
    "prettier/react",
    "plugin:jest/recommended",
  ],
  settings: {
    react: {
      pragma: "React",
      version: "detect",
    },
    "import/extensions": [".js", ".jsx", ".ts", ".tsx"],

    "import/parsers": {
      "@typescript-eslint/parser": [".ts", ".tsx"],
    },
    "import/resolver": {
      node: {
        extensions: [".js", ".jsx", ".ts", ".tsx", "json", ".d.ts"],
      },
    },
  },
  rules: {
    "react/jsx-props-no-spreading": "off",
    "react/prop-types": "off",
    "import/prefer-default-export": "off",
    "no-unused-vars": 2,
    "@typescript-eslint/no-unused-vars": 2,
    "no-use-before-define": ["error", { variables: false }],
    "@typescript-eslint/no-use-before-define": ["error", { variables: false }],
    "@typescript-eslint/explicit-member-accessibility": "off",
    "react/jsx-filename-extension": ["error", { extensions: [".tsx", ".ts", ".js", ".jsx"] }],
    "prettier/prettier": 2,
    "import/extensions": [
      "error",
      "ignorePackages",
      {
        js: "never",
        jsx: "never",
        ts: "never",
        tsx: "never",
      },
    ],
  },
  overrides: [
    {
      // Disable some rules in unit tests.
      files: ["test/**/*.ts", "test/**/*.tsx", "**/*.spec.ts", "**/*.spec.tsx"],
      rules: {
        "@typescript-eslint/no-non-null-assertion": "off",
        "@typescript-eslint/no-object-literal-type-assertion": "off",
      },
    },
  ],
};
