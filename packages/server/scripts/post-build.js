/* eslint-disable @typescript-eslint/no-var-requires */
// eslint-disable-next-line import/no-extraneous-dependencies
const fg = require("fast-glob");
const { copyFileSync, mkdirSync, existsSync } = require("fs");

async function main() {
  const entries = await fg("./src/**/*.graphql");

  entries.forEach((entry) => {
    const outputFile = entry.replace("./src", "./dist");

    const outputFolder = outputFile.substr(0, outputFile.lastIndexOf("/"));
    if (!existsSync(outputFolder)) {
      mkdirSync(outputFolder, { recursive: true });
    }

    copyFileSync(entry, outputFile);
  });
}

main();
