// eslint-disable-next-line import/no-extraneous-dependencies
import request from "supertest";
// eslint-disable-next-line import/no-extraneous-dependencies
import { createTestClient, ApolloServerTestClient } from "apollo-server-testing";
import { startServer } from "../server";

export interface ServerTest extends request.SuperTest<request.Test> {
  sendQL(query: string, variables?: object, token?: string): request.Test;
}

type CreateServer = ApolloServerTestClient & { close(): Promise<void> };

// eslint-disable-next-line import/prefer-default-export
export async function createServer(): Promise<CreateServer> {
  const { server, prisma } = await startServer();
  const testClient = createTestClient(server);

  async function close(): Promise<void> {
    await server.stop();
    await prisma.disconnect();
  }

  return { ...testClient, close };
}
