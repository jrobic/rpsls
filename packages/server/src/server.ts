import express from "express";
import { ApolloServer } from "apollo-server-express";
import { PrismaClient } from "@prisma/client";
import { StartServerResponse } from "./@types/Server";
import { ContextFn } from "./@types/Context";
import { executableSchema } from "./schema";
import "../config/env";

export const startServer = async (): Promise<StartServerResponse> => {
  const app = express();
  const prisma = new PrismaClient();

  const context: ContextFn = ({ req, res }) => ({
    req,
    res,
    prisma,
    env: process.env.NODE_ENV || "development",
  });

  const server = new ApolloServer({
    schema: executableSchema,
    context,
    playground: {
      settings: {
        "request.credentials": "include",
      },
    },
    tracing: true,
  });

  server.applyMiddleware({ app, cors: false });

  return {
    app,
    server,
    prisma,
  };
};
