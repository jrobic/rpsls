/* eslint-disable @typescript-eslint/explicit-function-return-type */
import git from "git-rev-sync";
import { DateTimeResolver } from "graphql-scalars";
import { Context } from "../../@types/Context";
import { Resolvers, Status } from "../../@types/generated";
import { name } from "../../utils/version";

const commonResolvers: Resolvers<Context> = {
  DateTime: DateTimeResolver,
  Query: {
    version(parent, args, ctx) {
      return {
        name,
        env: ctx.env,
        sha1: git.short("../../"),
        date: git.date(),
        version: git.tag(),
      };
    },
    async status(parent, args, ctx) {
      const res: Status = {
        prisma: "ok",
        uptime: Math.round(process.uptime()),
        timestamp: Date.now(),
        message: "ok",
      };

      try {
        await ctx.prisma.connect();
      } catch (error) {
        res.prisma = "nok";
      }
      return res;
    },
  },
};

export default commonResolvers;
