// eslint-disable-next-line import/no-extraneous-dependencies
import { ApolloServerTestClient } from "apollo-server-testing";
import { createServer } from "../../utils/testUtils";

// eslint-disable-next-line @typescript-eslint/no-var-requires
const packageJSON = require("../../../package.json");

const versionQuery = /* GraphQL */ `
  query versionQuery {
    version {
      name
      version
      date
      env
      sha1
    }
  }
`;

const statusQuery = /* GraphQL */ `
  query statusQuery {
    status {
      prisma
      message
      uptime
      timestamp
    }
  }
`;

// --------------------------------------------------------------------------
// TESTS
// --------------------------------------------------------------------------
describe("> Common", (): void => {
  let query: ApolloServerTestClient["query"];
  let close: () => Promise<void>;

  beforeAll(
    async (): Promise<void> => {
      ({ query, close } = await createServer());
    },
  );

  afterAll(async () => {
    await close();
  });

  test("should return api version", async (): Promise<void> => {
    const res = await query({ query: versionQuery });
    expect(res.data?.version.name).toEqual(packageJSON.name);
    expect(res.data?.version.version).not.toBeNull();
    expect(res.data?.version.date).not.toBeNull();
    expect(res.data?.version.env).toBe("test");
  });

  test("should return api status", async (): Promise<void> => {
    const res = await query({ query: statusQuery });
    expect(res.data?.status.prisma).toEqual("ok");
    expect(res.data?.status.message).toEqual("ok");
    expect(res.data?.status.timestamp).not.toBeNull();
    expect(res.data?.status.uptime).toBeGreaterThan(0);
  });
});
