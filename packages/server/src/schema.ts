import path from "path";
import { makeExecutableSchema } from "graphql-tools";
import { fileLoader, mergeTypes } from "merge-graphql-schemas";

// Types
const typesArray = fileLoader(path.join(__dirname, "./{modules,generated}/**/*.graphql"));
const typesMerged = mergeTypes(typesArray, { all: true });
const resolvers = fileLoader(path.join(__dirname, "./modules/**/resolvers.{ts,js}"));

const options = {
  typeDefs: typesMerged,
  resolvers,
  directiveResolvers: {},
};

export const executableSchema = makeExecutableSchema(options);
