import { PrismaClient } from "@prisma/client";
import { ExpressContext } from "apollo-server-express/dist/ApolloServer";
import { ContextFunction } from "apollo-server-core";

export type Context = {
  prisma: PrismaClient;
  env: string;
} & ExpressContext;

export type ContextFn = ContextFunction<ExpressContext, Context>;
