import { Express } from "express";
import { ApolloServer } from "apollo-server-express";
import { PrismaClient } from "@prisma/client";

export interface StartServerResponse {
  app: Express;
  server: ApolloServer;
  prisma: PrismaClient;
}
