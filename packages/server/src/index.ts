/* istanbul ignore file */
import { startServer } from "./server";
import { StartServerResponse } from "./@types/Server";

const port = process.env.PORT || 4000;

startServer().then(({ app }: StartServerResponse): void => {
  app.listen(port, () => {
    // eslint-disable-next-line no-console
    console.log(`Server started at http://localhost:${port}/graphql`);
  });
});
