import faker from "faker";
import times from "lodash/times";

import { PrismaClient, User } from "@prisma/client";

faker.seed(2020);

export default async function createUsers(): Promise<User[]> {
  const prisma = new PrismaClient();

  const users = Promise.all([
    ...times(
      100,
      async (): Promise<User> => {
        const user = {
          email: faker.internet.email().toLowerCase(),
          username: faker.internet.userName(),
          password: "$2b$10$dqyYw5XovLjpmkYNiRDEWuwKaRAvLaG45fnXE5b3KTccKZcRPka2m", // "secret42"
        };

        const userCreated = await prisma.user.upsert({
          where: { email: user.email },
          create: user,
          update: user,
        });

        return userCreated;
      },
    ),
  ]);

  await prisma.disconnect();

  return users;
}
