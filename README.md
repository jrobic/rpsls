[![pipeline status](https://gitlab.com/jrobic/rpsls/badges/develop/pipeline.svg)](https://gitlab.com/jrobic/rpsls/commits/develop)
[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/ce9113f70ef54081b418f84869eaac46)](https://www.codacy.com/manual/hello_98/rpsls?utm_source=gitlab.com&utm_medium=referral&utm_content=jrobic/rpsls&utm_campaign=Badge_Grade)
